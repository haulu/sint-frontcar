package hello.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Created by steha on 16.04.16.
 */
public class Booking {

    private Long carId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fromDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date toDate;

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }



}
