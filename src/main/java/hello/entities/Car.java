package hello.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by frost on 13.04.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car {



    private Long identifier;
    private int gasleft;
    private String gear;
    private String brand;
    private int price;
    private double longitude;
    private double latitude;
    private int filling;
    private String selfCarUrl;
    private String bookingCarUrl;

    public Car() {
    }

    public Long getIdentifier() {
        return identifier;
    }
    public String getGear() {
        return gear;
    }

    public void setGear(String gear) {
        this.gear = gear;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getFilling() {
        return filling;
    }

    public void setFilling(int filling) {
        this.filling = filling;
    }

    public String getSelfCarUrl() {
        return selfCarUrl;
    }

    public void setSelfCarUrl(String selfCarUrl) {
        this.selfCarUrl = selfCarUrl;
    }

    public String getBookingCarUrl() {
        return bookingCarUrl;
    }

    public void setBookingCarUrl(String bookingCarUrl) {
        this.bookingCarUrl = bookingCarUrl;
    }

    public int getGasleft() {
        return gasleft;
    }

    public void setGasleft(int gasleft) {
        this.gasleft = gasleft;
    }

    public String printCar (){
        return "ID: " + getIdentifier() + "\n" +
                "Brand: " + getBrand() + "\n" +
                "Gear: " + getGear() + "\n" +
                "Latitude: " + getLatitude() + "\n" +
                "Longitude: " + getLongitude() + "\n" +
                "Price: " + getPrice();
    }

}