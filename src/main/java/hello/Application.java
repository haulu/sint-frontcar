package hello;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import hello.entities.Booking;
import hello.entities.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.*;


@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        SpringApplication.run(Application.class);
    }

    @Bean
    public CommandLineRunner singleCar() {
        return (args) -> {

            RestTemplate restTemplate = new RestTemplate();
            Car car = restTemplate.getForObject("http://localhost:8080/cars/1", Car.class);
            log.info("-------------------------------");
            if (car == null) {
                log.info("No car found with id 1");
            } else {
                log.info(car.printCar());

            }
            log.info("");
            log.info("-------------------------------");
        };
    }

    @Bean
    public CommandLineRunner allCars() {
        return (args) -> {

            // HATEOAS - able template
            RestTemplate restTemplate = restTemplate();

            String url = "http://localhost:{port}/cars";

            ResponseEntity<Resources<Car>> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Resources<Car>>() {},
                    "8080");

            Resources<Car> resources = responseEntity.getBody();
            List<Car> cars = new ArrayList(resources.getContent());

            log.info("-------------------------------");
            log.info("Following cars found @ http://localhost:8080/cars");
            for (Car car : cars) {
                log.info(car.toString());
            }
            log.info("");
            log.info("-------------------------------");

        };
    }

    @Bean
    public CommandLineRunner allBookingsForCar() {
        return (args) -> {

            // HATEOAS - able template
            RestTemplate restTemplate = restTemplate();

            String url = "http://localhost:{port}/cars/1/bookings";

            ResponseEntity<Resources<Booking>> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Resources<Booking>>() {},
                    "8080");

            Resources<Booking> resources = responseEntity.getBody();
            List<Booking> bookings = new ArrayList(resources.getContent());

            log.info("-------------------------------");
            log.info("Following bookings found @ http://localhost:8080/cars/1/bookings");
            for (Booking booking : bookings) {
                booking.setCarId(Long.valueOf(1));
            }

            log.info("");
            log.info("-------------------------------");

        };
    }


    private RestTemplate restTemplate() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new Jackson2HalModule());

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(mapper);
        return new RestTemplate(Arrays.asList(converter));
    }
}